import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements ErrorHandler {

  constructor(private injector: Injector) { }
  handleError(error: any): void {
    console.log(error);
  }

  handleHttpError(error: HttpErrorResponse) {
    let errorTitle = error?.error || 'Error';
    const errorMessage = error?.error || 'Please try again later';
    if (error.status === 500) {
      errorTitle = 'Internal Server Error occured';
    }
    this._toastr.error(errorMessage, errorTitle);
    return throwError(error);
  }
  private get _toastr() {
    return this.injector.get(ToastrService);
  }
}
