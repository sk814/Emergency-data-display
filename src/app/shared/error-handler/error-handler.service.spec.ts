import { TestBed } from '@angular/core/testing';
import { ToastrService } from 'ngx-toastr';
import { ErrorHandlerService } from './error-handler.service';

describe('ErrorHandlerService', () => {
  let service: ErrorHandlerService;

  beforeEach(() => {
    const toastrSpy = jasmine.createSpyObj('ToastrService', [
      'error',
    ]);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ToastrService,
          useValue: toastrSpy,
        },
      ]
    });
    service = TestBed.inject(ErrorHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
