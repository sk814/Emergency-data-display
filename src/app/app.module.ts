import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { EmergencyDataModule } from './emergency-data/emergency-data.module';
import { SharedModule } from './shared/shared.module';
// import { ErrorHandlerService } from './shared/error-handler/error-handler.service';
import { TokenInterceptorService } from './shared/token-interceptor/token-interceptor.service';
import { ConfirmationService } from 'primeng/api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    EmergencyDataModule,
    SharedModule,
    HttpClientModule,
    ToastrModule.forRoot({
      closeButton: true,
      progressBar: true,
      positionClass :'toast-top-right'
    }),
  ],
  exports: [ToastrModule],
  providers: [
    ConfirmationService, 
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: TokenInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
