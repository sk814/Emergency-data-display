import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EmergencyDataService } from '../service/emergency-data.service';
import { TableModule } from 'primeng/table';
import { TabularEmeDataComponent } from './tabular-eme-data.component';
import { MockEmergencyDataService } from '../service/Mock-eme-data-service';

describe('TabularEmeDataComponent', () => {
  let component: TabularEmeDataComponent;
  let fixture: ComponentFixture<TabularEmeDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabularEmeDataComponent ],
      imports: [TableModule],
      providers: [
        {
          provide: EmergencyDataService,
          useClass: MockEmergencyDataService,
        },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabularEmeDataComponent);
    TestBed.inject(EmergencyDataService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.loading).toBe(false);
  });
  it('should get data', () => {
    expect(component.emergencyData?.length).toBeGreaterThan(1);
  });
  it('should stop loading', () => {
    expect(component.loading).toBe(false);
  });
});
