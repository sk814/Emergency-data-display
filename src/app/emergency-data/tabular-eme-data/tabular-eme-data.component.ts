import { Component, OnInit } from '@angular/core';
import { EmergencyData } from '../model';
import { EmergencyDataService } from '../service/emergency-data.service';

@Component({
  selector: 'app-tabular-eme-data',
  templateUrl: './tabular-eme-data.component.html',
  styleUrls: ['./tabular-eme-data.component.scss']
})
export class TabularEmeDataComponent implements OnInit {
  emergencyData : [EmergencyData] | any;
  loading = true;

  constructor(private emergencyDataService: EmergencyDataService) { }

  ngOnInit(): void {
    this.getEmergencyData();
  }

  getEmergencyData() {
    this.emergencyDataService.getAllEmergencyData().subscribe((res) => {
      this.emergencyData = res.content;
      this.loading = false;
    },
      () => {
        // In case of error
        this.loading = false;
      }
    );
  }

}
