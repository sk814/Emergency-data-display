export interface EmergencyData {
  content: [
    emergency: {
      emergencyId: string,
      requestTime: Date
    },
    device: {
      serialNumber: string
    },
    holder: {
      firstName: string,
      lastName: string
    }
  ]
}
  