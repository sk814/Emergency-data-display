import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabularEmeDataComponent } from './tabular-eme-data/tabular-eme-data.component';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    TabularEmeDataComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    TableModule,
  ],
  exports: [
    TabularEmeDataComponent
  ]
})
export class EmergencyDataModule { }
