import { EmergencyData } from "../model";

export const CONTENT_DUMMY_DATA: [EmergencyData] | any = {
    "content": [{
        "emergency": {
            "emergencyId": "2f7d8518-31be-4d81-ba7a-de2abgc2c498",
            "requestTime": "2021-11-08T14:04:52.365+00:00"
        },
        "device": {
            "serialNumber": "RFAZ22FWAKZ"
        },
        "holder": {
            "firstName": "Jonah",
            "lastName": "Johnson"
        }
    },
    {
        "emergency": {
            "emergencyId": "38f47199-df33-4104-af09-1ba8535ce9de",
            "requestTime": "2021-11-08T14:04:51.047+00:00"
        },
        "device": {
            "serialNumber": "2RWFZ2NKAFE"
        },
        "holder": {
            "firstName": "Jonah",
            "lastName": "Johnson"
        }
    },
    ]
}