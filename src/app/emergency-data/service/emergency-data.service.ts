import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ErrorHandlerService } from 'src/app/shared/error-handler/error-handler.service';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { EmergencyData } from '../model';

@Injectable({
  providedIn: 'root'
})
export class EmergencyDataService {

  // Not returning any url, Used for best practice
  private API_URL = environment.API_URL;

  constructor(private httpClient: HttpClient,private errorHandler: ErrorHandlerService) { }

  getAllEmergencyData() {
    return this.httpClient
      .get<EmergencyData>(`${this.API_URL}/getAllEmergencies`, {
        responseType: 'json',
      })
      .pipe(catchError((error) => this.errorHandler.handleHttpError(error)));
  }
}
