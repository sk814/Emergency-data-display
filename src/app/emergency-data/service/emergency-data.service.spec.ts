import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { EmergencyDataService } from './emergency-data.service';
import { MockEmergencyDataService } from './Mock-eme-data-service';

describe('EmergencyDataService', () => {
  let service: EmergencyDataService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: EmergencyDataService,
          useClass: MockEmergencyDataService,
        },
      ]
    });
    service = TestBed.inject(EmergencyDataService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should retrive all data', () => {
    service.getAllEmergencyData().subscribe(data => {
      expect(data).toBeTruthy("No Emergency data returned!");
      expect(data.content.length).toBeGreaterThanOrEqual(2, 'Less data returned');
    });

    // const req = httpTestingController.expectOne('/getAllEmergencies');
    // expect(req.request.method).toEqual('GET');
  });
  
});
