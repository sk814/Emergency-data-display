import { of } from 'rxjs';
import { CONTENT_DUMMY_DATA } from './test-data';

export class MockEmergencyDataService {

    getAllEmergencyData() {
      return of(CONTENT_DUMMY_DATA);
  }
}