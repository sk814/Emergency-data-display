# EmergencyDataDisplay

This project was generated with [Angular CLI].

# Pull from git repo

#### run `git clone https://gitlab.com/sk814/Emergency-data-display.git` to clone the project.

####  run `npm install` in the project directory to get all required packages.

####  Run `ng serve` for a dev server. 
    
####  Navigate to `http://localhost:4200/`. 

#### Overview
 ![Getting Started](./emergency-data.png)

 #### Infield filtering
 ![Getting Started](./emergency-data2.png)

 #### Multiple sorting
 ![Getting Started](./emergency-data3.png)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
